var express = require('express');
var router = express.Router();
const passport = require('../passport')

router.get('/login', (req, res, next) => {
  res.render('login', { title: 'Express' });
});

// test api
// when calling api protected, you need to put the access token in the headers of request
router.get('/users', passport.authenticate('oauth-bearer', { session: false }), (req, res, next) => {
  res.json({
    success: true
  })
});

module.exports = router;

const passport = require('passport')
const BearerStrategy = require('passport-azure-ad').BearerStrategy

const tenantName = 'speechlab' // change this to match your setup
const tenantID = tenantName + '.onmicrosoft.com'
const clientID = '4346ccbe-c01c-41cc-b585-5193a6c05c8d' // change this to match your setup
const policyName = 'B2C_1_Speechlab-Authentication' // change this to match your setup
const domain = 'login.microsoftonline.com'

const options = {
  identityMetadata: 'https://' + domain + '/' + tenantID + '/v2.0/.well-known/openid-configuration/',
  clientID: clientID,
  policyName: policyName,
  isB2C: true,
  validateIssuer: true,
  loggingLevel: 'info',
  passReqToCallback: false
}

const bearerStrategy = new BearerStrategy(options, (token, done) => {
  // Send user info using the second argument
  done(null, { }, token)
})

passport.use(bearerStrategy)
module.exports = passport
